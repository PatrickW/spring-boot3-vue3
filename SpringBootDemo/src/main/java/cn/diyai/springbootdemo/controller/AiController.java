package cn.diyai.springbootdemo.controller;

import org.springframework.ai.chat.ChatClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AiController {

    @Autowired
    ChatClient chatClient;

    @GetMapping("/ai")
    String generation(@RequestParam("msg") String msg) {
        return this.chatClient.call(msg);
    }
}
