package com.example.hello.controller;

import com.example.hello.dto.CommonResponse;
import com.example.hello.dto.UserLoginRequest;
import com.example.hello.dto.UserRegisterRequest;
import com.example.hello.entity.User;
import com.example.hello.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/user")
@CrossOrigin(originPatterns = {"http://localhost:[*]", "http://127.0.0.1:[*]"}, maxAge = 3600, allowCredentials = "true")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/register")
    public CommonResponse<User> register(@RequestBody UserRegisterRequest request) {
        // 检查用户名是否已存在
        if (userRepository.findByUsername(request.getUsername()) != null) {
            return CommonResponse.error("用户名已存在");
        }

        User user = new User();
        user.setUsername(request.getUsername());
        user.setPassword(request.getPassword());  // 实际项目中应该对密码进行加密
        user.setEmail(request.getEmail());
        user.setCreateTime(LocalDateTime.now().toString());

        User savedUser = userRepository.save(user);
        savedUser.setPassword(null);  // 返回结果中不包含密码
        return CommonResponse.success(savedUser);
    }

    @PostMapping("/login")
    public CommonResponse<User> login(@RequestBody UserLoginRequest request) {
        User user = userRepository.findByUsername(request.getUsername());
        if (user == null) {
            return CommonResponse.error("用户不存在");
        }

        if (!user.getPassword().equals(request.getPassword())) {  // 实际项目中应该对密码进行加密比较
            return CommonResponse.error("密码错误");
        }

        user.setPassword(null);  // 返回结果中不包含密码
        return CommonResponse.success(user);
    }

    @GetMapping("/{id}")
    public CommonResponse<User> getUserById(@PathVariable String id) {
        User user = userRepository.findById(id).orElse(null);
        if (user == null) {
            return CommonResponse.error("用户不存在");
        }

        user.setPassword(null);  // 返回结果中不包含密码
        return CommonResponse.success(user);
    }
} 