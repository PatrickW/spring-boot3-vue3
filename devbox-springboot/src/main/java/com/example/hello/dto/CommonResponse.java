package com.example.hello.dto;

import lombok.Data;

@Data
public class CommonResponse<T> {
    private int code;
    private String message;
    private T data;

    public static <T> CommonResponse<T> success(T data) {
        CommonResponse<T> response = new CommonResponse<>();
        response.setCode(200);
        response.setMessage("success");
        response.setData(data);
        return response;
    }

    public static <T> CommonResponse<T> error(String message) {
        CommonResponse<T> response = new CommonResponse<>();
        response.setCode(500);
        response.setMessage(message);
        return response;
    }
} 