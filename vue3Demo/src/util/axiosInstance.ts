import axiosAPI from 'axios'

const axios = axiosAPI.create({
  baseURL: '/api',//http://localhost:8080',
  timeout: 1000,
  headers: {
    'Content-Type': 'application/json',
  },
})

export default axios
